import adapter from '@sveltejs/adapter-static';
import { vitePreprocess } from '@sveltejs/kit/vite';

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    adapter: adapter({
      pages: 'public',
      assets: 'public',
      fallback: 'index.html'
    }),
    alias: {
      $data: "./src/data"
    },
    paths: {
      assets: '',
      base: '/cvr'
    }
  },
  preprocess: vitePreprocess()
};

export default config;